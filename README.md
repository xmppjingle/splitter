# Splitter #

Splitter is a micro service that splits Messages into Mentions, Emojis and Links

### Build and Run ###

The following command will build and run the service on default port 8080
```shell
./gradlew clean build bootRun

```

### Usage ###

The REST POST Request SHOULD be done on [ADDRESS]/split and the input SHOULD be done through Request Body Raw Data.
The Response is an application/json;charset=UTF-8 Encoded Object.

```shell
curl -X POST -d '@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016' "http://localhost:8080/split"
```

Response:
```json
{
  "mentions": [
    "bob",
    "john"
  ],
  "emoticons": [
    "success"
  ],
  "links": [
    {
      "url": "https://twitter.com/jdorfman/status/430511497475670016",
      "title": "Justin Dorfman op Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;"
    }
  ]
}
```