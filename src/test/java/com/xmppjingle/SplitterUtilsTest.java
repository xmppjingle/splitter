package com.xmppjingle;

import com.xmppjingle.model.Link;
import com.xmppjingle.util.Utils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by thiago on 11/17/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class SplitterUtilsTest {

    @Test
    @Repeat(100)
    public void betweenCharString() throws Exception {

        String txt1 = "Testing (hotdog) with (cookies) on (marzipan_van_si) (long_strings_are_not_emoji)" + (System.currentTimeMillis()) + "(emomoji)";
        assertThat(Utils.inBetween('(', ")", 15, txt1).get(0)).isEqualTo("hotdog");
        assertThat(Utils.inBetween('(', ")", 15, txt1).get(1)).isEqualTo("cookies");
        assertThat(Utils.inBetween('(', ")", 15, txt1).get(2)).isEqualTo("marzipan_van_si");
        assertThat(Utils.inBetween('(', ")", 15, txt1).get(3)).isEqualTo("emomoji");
        assertThat(Utils.inBetween('(', ")", 15, txt1).size()).isEqualTo(4);

        String txt2 = "Testing (hotdog) with (cookies) on (marzipan_van_si) for @luke not for @-dark! on @helicopter, looks @handy. (long_strings_are_not_emoji)" + (System.currentTimeMillis()) + "(emomoji)";
        assertThat(Utils.inBetween('@', " !?,.;'`$%ˆ*)(", 15, txt2).get(0)).isEqualTo("luke");
        assertThat(Utils.inBetween('@', " !?,.;'`$%ˆ*)(", 15, txt2).get(1)).isEqualTo("-dark");
        assertThat(Utils.inBetween('@', " !?,.;'`$%ˆ*)(", 15, txt2).get(2)).isEqualTo("helicopter");
        assertThat(Utils.inBetween('@', " !?,.;'`$%ˆ*)(", 15, txt2).get(3)).isEqualTo("handy");
        assertThat(Utils.inBetween('@', " !?,.;'`$%ˆ*)(", 15, txt2).size()).isEqualTo(4);

    }

    @Test
    @Repeat(100)
    public void extractEmojis() throws Exception {
        String txt1 = "Testing (hotdog) with ((cookies)) on ((marzipanvansi) (no(t!emoj)i) (long_strings_are_not_emoji)" + (System.currentTimeMillis()) + "(emomoji)";
        List<String> emojis = Utils.getEmojis(txt1);
        assertThat(emojis.get(0)).isEqualTo("hotdog");
        assertThat(emojis.get(1)).isEqualTo("cookies");
        assertThat(emojis.get(2)).isEqualTo("marzipanvansi");
        assertThat(emojis.get(3)).isEqualTo("emomoji");
        assertThat(emojis.size()).isEqualTo(4);

    }

    @Test
    @Repeat(100)
    public void extractMentions() throws Exception {
        String txt2 = "Testing (hotdog) with (cookies) on ((marzipan_van_si)) for @@luke not for @-dark! on @helicopter, looks @handy. (long_strings_are_not_emoji)" + (System.currentTimeMillis()) + "(emomoji)";
        List<String> mentions = Utils.getMentions(txt2);
        assertThat(mentions.get(0)).isEqualTo("luke");
        assertThat(mentions.get(1)).isEqualTo("helicopter");
        assertThat(mentions.get(2)).isEqualTo("handy");
        assertThat(mentions.size()).isEqualTo(3);

    }

    @Test
    @Repeat(10)
    public void extractLinks() throws Exception {
        String txt3 = "Testing (hotdog) with (cookies) on (marzipan_van_si) http://www.google.com for @luke not for @-dark http://google.com(---)# ! on @helicopter, looks @handy. (long_strings_are_not_emoji)" + (System.currentTimeMillis()) + "(emomoji)";
        List<Link> links = Utils.getLinks(txt3);
        assertThat(links.get(0).getUrl()).isEqualTo("http://www.google.com/");
        assertThat(links.get(0).getTitle()).isEqualTo("Google");
        assertThat(links.get(1).getUrl()).isEqualTo("http://google.com/");
        assertThat(links.get(1).getTitle()).isEqualTo("Google");
        assertThat(links.size()).isEqualTo(2);
    }

}
