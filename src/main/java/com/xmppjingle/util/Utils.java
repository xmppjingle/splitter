package com.xmppjingle.util;

import com.xmppjingle.model.Link;
import com.linkedin.urls.Url;
import com.linkedin.urls.detection.UrlDetector;
import com.linkedin.urls.detection.UrlDetectorOptions;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by thiago on 11/17/16.
 */
public class Utils {

    /**
     * High Performance single level extractor of Strings in between a char and a CharFilter, applying an StringFilter that validate if each entry validity.
     * Single Level extraction prevents issues with recursive delimitation that might occur on ReGex based extractions etc...
     * @param a Beginning char. The char that marks the start of a in between delimited entry
     * @param charFilter The filter that defines if a given char is a termination delimiter
     * @param maxLength Max length of a valid entry. If length is exceeded the entry is not included in the return
     * @param filter The String Filter that validates if the matched delimitation is valid
     * @param str The String to be processed
     * @return
     */
    public static List<String> inBetween(char a, CharFilter charFilter, int maxLength, StringFilter filter, String str) {
        int i = 0;
        int j = -1;
        final List<String> entries = new ArrayList<>();
        for (; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c == a) {
                j = i;
            } else if (j != -1 && charFilter.isValid(c)) {
                if (i - j - 1 > 0 && i - j - 1 <= maxLength) {
                    final String e = str.substring(j + 1, i);
                    if (filter != null && filter.isValid(e)) {
                        entries.add(e);
                    }
                }
                j = -1;
            }
        }
        return entries;
    }

    public static List<String> inBetween(char a, String bs, int maxLength, String str) {
        return inBetween(a, (c) -> bs.indexOf(c) > -1, maxLength, (s) -> true, str);
    }

    /**
     * Get emojis from a given String.
     * Emojis are alphanumeric words not longer than 15 chars.
     *
     * @param str String to get Emojis from
     * @return List of Emoji Strings
     */
    public static List<String> getEmojis(String str) {
        return inBetween('(', (c) -> c == ')', 15, StringUtils::isAlphanumeric, str);
    }

    /**
     * Get Mentions from a given String.
     * Mentions are preceded by '@' and terminated by a non-word char.
     * A word character is a character from a-z, A-Z, 0-9, including the _ (underscore) character.
     *
     * @param str
     * @return List of Mention Strings
     */
    public static List<String> getMentions(String str) {
        return inBetween('@', (c) -> !Character.isDigit(c) && !Character.isLetter(c) && c != '_', 30, (s) -> true, str);
    }

    /**
     * Get Links from a given String.
     * Links are found matching a Browser behaviour URL Detection.
     *
     * @see <a href="https://github.com/linkedin/URL-Detector">URL Detector</a>
     *
     * @param str
     * @return List of Links
     */
    public static List<Link> getLinks(String str) {
        UrlDetector parser = new UrlDetector(str, UrlDetectorOptions.Default);
        return parser.detect().parallelStream().map(Url::getFullUrl).map((u) -> {
            try {
                return new Link(u, StringEscapeUtils.escapeHtml4(Jsoup.connect(u).get().title()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return new Link(u, "");
        }).collect(Collectors.toList());
    }

    public interface CharFilter {
        boolean isValid(char c);
    }

    public static interface StringFilter {
        boolean isValid(String str);
    }
}
