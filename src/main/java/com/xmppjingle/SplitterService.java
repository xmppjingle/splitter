package com.xmppjingle;

import com.xmppjingle.model.SplitResponse;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;

/**
 * Created by thiago on 11/17/16.
 */
@RestController
@EnableAutoConfiguration
public class SplitterService {

    @RequestMapping(value = "/split", method = RequestMethod.POST)
    public
    @ResponseBody
    SplitResponse splitter(@RequestBody String msg){
        return new SplitResponse(msg);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SplitterService.class, args);
    }

}
