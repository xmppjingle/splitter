package com.xmppjingle.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by thiago on 11/17/16.
 */
public class Link {
    private String url;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String title;

    public Link(String link, String title) {
        this.url = link;
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
