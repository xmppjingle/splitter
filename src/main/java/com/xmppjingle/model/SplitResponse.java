package com.xmppjingle.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.xmppjingle.util.Utils;

import java.util.List;

/**
 * Created by thiago on 11/17/16.
 */
public class SplitResponse {
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<String> mentions;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<String> emoticons;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<Link> links;

    public SplitResponse(final String msg) {
        this(Utils.getMentions(msg), Utils.getEmojis(msg), Utils.getLinks(msg));
    }

    public SplitResponse(List<String> mentions, List<String> emoticons, List<Link> links) {
        this.mentions = mentions;
        this.emoticons = emoticons;
        this.links = links;
    }

    public List<String> getMentions() {
        return mentions;
    }

    public void setMentions(List<String> mentions) {
        this.mentions = mentions;
    }

    public List<String> getEmoticons() {
        return emoticons;
    }

    public void setEmoticons(List<String> emoticons) {
        this.emoticons = emoticons;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }
}
